from django.shortcuts import render, redirect
from constructorasApp.models import *

def constructoras_base(request):
    #Consulto todos los inmueble ordenados de mas nuevo a mas viejo
    inmueble = Inmueble.objects.all().order_by('-publicacion')

    #Consulto la informacion de la empresa
    empresa = Empresa.objects.first()

    #Vivienda mas costosa
    qs = Inmueble.objects.all().order_by('-precio')
    mas_costoso = qs.first()
    mas_varato = qs.last()
    tipos_inmueble = TipoVivienda.objects.all().order_by('-nombre')

    #Armo el context con la informacion que necesito para la base.
    context = {'inmueble': inmueble, 
               'empresa':empresa,
               'mas_costoso': mas_costoso,
               'mas_varato': mas_varato,
               'tipos_inmueble':tipos_inmueble,
            }

    return render(request, 'base.html', context)