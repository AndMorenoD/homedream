from django.shortcuts import render, redirect
from .models import *

def filter(request):

    #Consulto la informacion de la empresa
    empresa = Empresa.objects.first()

    #Consulto todos los inmueble ordenados de mas nuevo a mas viejo
    inmueble = Inmueble.objects.all()

    #Consulto los tipos de inmueble existentes en db
    tipos_inmueble = TipoVivienda.objects.all().order_by('-nombre')

    #Vivienda mas costosa
    qs = Inmueble.objects.all().order_by('-precio')
    mas_costoso = qs.first()
    mas_varato = qs.last()

    #Filtro de busqueda 
    direccion_form = request.GET.get('direccion')
    tipo_vivienda = request.GET.get('tipo_vivienda')
    precios = request.GET.get('precio')
    espacio = request.GET.get('espacio')
    flag = False
    
    if direccion_form != '' and direccion_form is not None and tipo_vivienda != '' and tipo_vivienda is not None:
        filtro_qs = inmueble.filter(direccion__icontains = direccion_form, tipo__pk = tipo_vivienda )
        if len(filtro_qs) == 0:
            filtro_qs = inmueble
            flag = True
    elif direccion_form != '' and direccion_form is not None:
        filtro_qs = inmueble.filter(direccion__icontains = direccion_form)
        if len(filtro_qs) == 0:
            filtro_qs = inmueble
            flag = True
    elif tipo_vivienda != '' and tipo_vivienda is not None:
        filtro_qs = inmueble.filter(tipo__pk = tipo_vivienda )
        if len(filtro_qs) == 0:
            filtro_qs = inmueble
            flag = True
    elif precios != '' and precios is not None:
        min_max = precios.split(',')
        min = int(min_max[0]) 
        max = int(min_max[1])
        filtro_qs = inmueble.filter(precio__range = (min,max) )
        if len(filtro_qs) == 0:
            filtro_qs = inmueble
            flag = True
    elif espacio != '' and espacio is not None:
        espacio = espacio.split(',')
        min = int(espacio[0])
        max = int(espacio[1])
        filtro_qs = inmueble.filter(espacio__range = (min,max))
        if len(filtro_qs) == 0:
            filtro_qs = inmueble
            flag = True
    else:
        filtro_qs = inmueble.order_by('?')
        flag = True
    
    #Armo el context con la informacion que necesito para la base.
    context = {
               'tipos_inmueble':tipos_inmueble,
               'filtro_qs': filtro_qs,
               'mas_costoso': mas_costoso,
               'mas_varato': mas_varato,
               'empresa':empresa,
               'flag': flag
            }
    return render(request, 'filterTemplate.html', context)

def constructoras_base(request):
    #Consulto todos los inmueble ordenados de mas nuevo a mas viejo
    inmueble = Inmueble.objects.all().order_by('-publicacion')

    #Consulto la informacion de la empresa
    empresa = Empresa.objects.first()

    #Vivienda mas costosa
    qs = Inmueble.objects.all().order_by('-precio')
    mas_costoso = qs.first()
    mas_varato = qs.last()
    tipos_inmueble = TipoVivienda.objects.all().order_by('-nombre')

    #Armo el context con la informacion que necesito para la base.
    context = {'inmueble': inmueble, 
               'empresa':empresa,
               'mas_costoso': mas_costoso,
               'mas_varato': mas_varato,
               'tipos_inmueble':tipos_inmueble,
            }

    return render(request, 'base.html', context)

def item_detail(request, pk):
    item = Inmueble.objects.get(id= pk)
    qs = Inmueble.objects.all().order_by('-precio')
    mas_costoso = qs.first()
    mas_varato = qs.last()
    tipos_inmueble = TipoVivienda.objects.all().order_by('-nombre')
    recomendados = Inmueble.objects.all().order_by('precio')[:4]
    empresa = Empresa.objects.first()
    context = {'mas_costoso': mas_costoso,
               'mas_varato': mas_varato,
               'tipos_inmueble':tipos_inmueble,
                'item': item, 'empresa':empresa,
                'recomendados':recomendados}
    return render(request, 'carreteFotos.html', context)



