from django.contrib import admin
from .models import *

#Configuracion para las imagenes de Inmuebles
class ImagenesInLine(admin.TabularInline):
    model = Inmueble.imagen.through
    
class InmuebleAdmin(admin.ModelAdmin):
    inlines = [ImagenesInLine,]
    exclude = ()

#Configuracion para las imagenes del banner de las empresas
class ImagenesBannerInLine(admin.TabularInline):
    model = Empresa.banners.through
    
class EmpresaAdmin(admin.ModelAdmin):
    inlines = [ImagenesBannerInLine,]
    exclude = ()


admin.site.register(Vendedor)
admin.site.register(Cercanias)
admin.site.register(TipoVivienda)
admin.site.register(Inmueble , InmuebleAdmin)
admin.site.register(Empresa , EmpresaAdmin)
admin.site.register(Imagenes)






