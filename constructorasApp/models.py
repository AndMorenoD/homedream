from django.contrib.auth.models import User
from django.db import models
from djmoney.models.fields import MoneyField

class Imagenes(models.Model):

    nombre = models.CharField(max_length = 100)
    imagen = models.ImageField(upload_to='fotos/')

    def __str__(self):
        return self.nombre + " " + self.imagen.url

class Empresa(models.Model):

    usuario = models.OneToOneField(User, on_delete=models.CASCADE)
    titulo = models.CharField(max_length=100)
    nombre = models.CharField(max_length = 100)
    logo = models.ImageField(upload_to='empresasLogos/')
    eslogan = models.TextField(max_length=1000)
    banners = models.ManyToManyField(Imagenes , through= 'ImgBanners')
    facebook = models.CharField(max_length = 100)
    twitter = models.CharField(max_length = 100)
    linkedin = models.CharField(max_length = 100)
    instagram = models.CharField(max_length = 100)
    correo = models.CharField(max_length = 100)
    telefono = models.CharField(max_length = 100)
    direccion  = models.CharField(max_length = 100)

    def __str__(self):
        return self.nombre
    
class ImgBanners(models.Model):
    id_img = models.ForeignKey(Imagenes, on_delete = models.CASCADE)
    id_empresa = models.ForeignKey(Empresa , on_delete = models.CASCADE)

class TipoVivienda(models.Model):
    nombre = models.CharField(max_length=100)

    def __str__(self):
        return self.nombre

class Vendedor(models.Model):
    identificacion = models.CharField(max_length=100)
    img = models.ImageField(upload_to='fotosVendedores/')
    nombre = models.CharField(max_length=100)
    apellido = models.CharField(max_length=100)
    telefono = models.CharField(max_length=100)
    celular = models.CharField(max_length=100)
    correo  = models.EmailField(max_length=100)
    facebook = models.CharField(max_length = 100)
    twitter = models.CharField(max_length = 100)
    linkedin = models.CharField(max_length = 100)
    instagram = models.CharField(max_length = 100)
    oficina = models.CharField(max_length = 100)
    mensaje = models.TextField(max_length = 100, help_text="Mensaje de acercamiento o cualidades del vendedor")

    def __str__(self):
        return self.nombre + " " +  self.apellido + " ID:" + self.identificacion

class Cercanias(models.Model):
    nombre = models.CharField(max_length=200)
    link = models.URLField(blank=True, null=True)

    def __str__(self):
        return self.nombre

class Inmueble(models.Model):
    publicacion = models.DateTimeField(auto_now_add=True)
    img_principal = models.ImageField(upload_to='fotos/')
    tipo = models.ForeignKey(TipoVivienda, on_delete = models.CASCADE)
    vendedor = models.ForeignKey(Vendedor, on_delete = models.CASCADE)
    espacio = models.IntegerField(help_text="La unidad es en M2")
    antiguedad = models.CharField(max_length=100, help_text="Año de construccion de la propiedad." )
    direccion = models.CharField(max_length=100)
    video = models.CharField(max_length=2000 )
    nombre = models.CharField(max_length= 100 , help_text="Ingresar un identificador para el inmueble - Nombre + Edificio al que pertenece")
    numero = models.CharField(max_length = 500, help_text = "Numero en el edificio" )
    alcobas = models.CharField(max_length=100 , help_text = "Breve despcripcion de las alcobas")
    baño = models.CharField(max_length=100 , help_text = "Breve despcripcion de los baños")
    nuevo = models.BooleanField(default = False)
    garaje = models.BooleanField(default = False)
    terraza = models.BooleanField(default = False)
    comedor = models.BooleanField(default = False)
    cocina = models.BooleanField(default = False)
    estado = models.BooleanField(default = False)
    psicina = models.BooleanField(default = False)
    frente_al_mar = models.BooleanField(default=False)
    precio = MoneyField(max_digits=14, decimal_places=0, default_currency='USD')
    descripcion_general = models.TextField(max_length=500, blank = True)
    vista = models.TextField(max_length=1000)
    detalles_parqueadero = models.TextField(max_length=1000)
    imagen = models.ManyToManyField(Imagenes , through= 'ImgInmueble')
    cercanias = models.ManyToManyField(Cercanias)
    
    precio_no_format = models.IntegerField()

    def __str__(self):
        return self.nombre + " | Vendedor: " + self.vendedor.nombre

class ImgInmueble(models.Model):
    id_img = models.ForeignKey(Imagenes, on_delete = models.CASCADE)
    id_inmueble = models.ForeignKey(Inmueble , on_delete = models.CASCADE)








