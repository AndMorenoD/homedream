from django.contrib import admin
from django.urls import path
from constructorasApp import views


urlpatterns = [
	
    path('',views.constructoras_base,name='base'),
    path('filtro',views.filter,name='filtro'),
    path('item/<int:pk>/',views.item_detail,name='item-detail'),

]